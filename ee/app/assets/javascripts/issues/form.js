import initEpicsSelect from 'ee/vue_shared/components/sidebar/epics_select/epics_select_bundle';
import initForm from '~/issues/form';

export default () => {
  initEpicsSelect();
  initForm();
};
